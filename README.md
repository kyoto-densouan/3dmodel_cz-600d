# README #

1/3スケールのSHARP X-1turboZ/X68000用ディスプレイ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。
背面は情報が少ないため実機と異なると思います。
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

組み込む液晶パネルは以下を想定しています。
5V駆動4.3インチHDMIモニター自作セット [SET-6802V2-L043] 
http://www.aitendo.com/product/13740 

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-600d/raw/ad599f6f671869936986294e8df170e773ca49fc/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-600d/raw/ad599f6f671869936986294e8df170e773ca49fc/ModelView_open.png)
